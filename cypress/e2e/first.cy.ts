describe('template spec', () => {
  let maVariable:number;
  beforeEach(() => {
    maVariable = 10;
  });
  it('passes', () => {
    maVariable++;
    expect(maVariable).to.equal(11);
    cy.visit('/')
    cy.contains('Bouton 1').should('exist');
  })
})