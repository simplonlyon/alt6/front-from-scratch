describe('The counter page', () =>{

    it('should display a working counter', () => {

        cy.visit('/exo-counter.html');
        cy.contains('3').should('exist');
        cy.contains('+').click();
        cy.contains('4').should('exist');
        cy.contains('-').click();
        cy.contains('-').click();
        cy.contains('2').should('exist');
    });

});