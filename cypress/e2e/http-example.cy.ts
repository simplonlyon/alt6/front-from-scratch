describe('Show http stubbing', () => {
    it('should make a false request', () => {
        cy.intercept('http://localhost:8080/dogs', {fixture: 'getDogs.json'});
        cy.visit('/exo-http.html');
    });
})