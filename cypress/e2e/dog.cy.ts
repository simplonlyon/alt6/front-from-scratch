describe('Dog Page', () => {

    it('should display the dogs', () => {
        cy.intercept('GET', 'http://localhost:8080/dogs', {fixture: 'getDogs.json'}).as('fetchDogs');
        cy.intercept('POST', 'http://localhost:8080/dogs', {body: {id:3, name: 'Nom', breed:'Race', birthDate:'2023-01-01'}})
        cy.visit('/exo-dog.html');

        cy.contains('Fido').should('exist');
        cy.contains('Rex').should('exist');

        cy.contains('label', 'Name').type('Nom');
        cy.contains('label', 'Breed').type('Race');
        cy.contains('label', 'birthDate').type('2023-01-01');

        cy.contains('Submit').click();

        cy.contains('Nom').should('exist');
        // cy.get('simplon-dog').shadow().get('simplon-dog-form');
    });
});