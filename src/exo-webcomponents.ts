import { CounterWebComponent } from "./components/CounterWebComponent";
import { FirstWebComponent } from "./components/FirstWebComponent";
import { PersonComponent } from "./components/PersonComponent";



customElements.define('simplon-first', FirstWebComponent);
customElements.define('simplon-counter', CounterWebComponent);
customElements.define('simplon-person', PersonComponent);