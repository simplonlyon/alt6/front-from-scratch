import { Counter } from "./components/Counter";



let validator = {
    set: function(target:any, key:string, value:any, receiver:any) {
        target[key] = value;
        if(target.render) {
            receiver.render();
        }
        return true;
    }
};



let tag = '#app';

const app = document.querySelector(tag);

app?.querySelectorAll('Counter').forEach(element => {
    /**
     * Ici, on utilise une autre manière que dans l'AbstractComponent pour bénéficier 
     * de la réactivité, on fait en sorte d'utiliser un Proxy (un objet qui vient prendre
     * la place de l'objet original en permettant de lui rajouter des fonctionnalités
     * supplémentaire). Et ce Proxy est configuré au dessus pour faire en sorte que 
     * dès qu'une propriété de l'objet cible est modifiée, on redéclenche la méthode render
     * de celui ci
     */
    const instance = new Proxy(new Counter(), validator);
    instance.increment();
    instance.increment();
    instance.increment();
    element.replaceWith(instance.render());
});