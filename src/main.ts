import './basics/callback';
import { firstFunction, showPerson } from "./functions-example";
import './basics/dom';
import './basics/Person';

let maVariableNumerique:number = 10;
let maVariableString:string = 'coucou';
// let maVariableBoolean:boolean;


let tableau:number[] = [10,2,4];

let objetInline = {
  name:'Jean',
  greeting() {
    console.log(this.name);
  }
};

objetInline.greeting();


if(maVariableNumerique < 10) {

} else {

}

for (let index = 0; index < 10; index++) {
  console.log(index);
  
}

for (const item of tableau) { //for(int item : tableau) {}
  console.log(item);
}

firstFunction(maVariableString)

showPerson({age: 10, name: 'test'});