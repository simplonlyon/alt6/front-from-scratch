import { Dog } from "../Dog";


export class DogService {
    private url = 'http://localhost:8080/dogs';

    async fetchAll(): Promise<Dog[]> {
        const response = await fetch(this.url);
        const body = await response.json();
        
        return body._embedded.dogs.map(this.jsonToDog);
    }

    async post(dog:Dog):Promise<Dog> {
        const response = await fetch(this.url, {
            method: 'POST',
            body: JSON.stringify(dog),
            headers: {
                'Content-type':'application/json'
            }
        });
        const body = await response.json();
        return this.jsonToDog(body);
    }

    private jsonToDog(json:any):Dog {
        
        return {
            ...json,
            birthDate: json.birthDate ? new Date(json.birthDate):undefined
        }
    }
}