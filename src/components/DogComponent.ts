import { Dog } from "../Dog";
import { DogService } from "../services/DogService";
import { AbstractComponent } from "./AbstractComponent";
import "./DogFormComponent";



interface State {
    dogs: Dog[];
}

class EventDataLoaded extends Event{
    constructor(public data:any) {
        super('data-loaded')
    }
}

export class DogComponent extends AbstractComponent<State> {
    private service = new DogService();

    constructor() {
        super();
        
        this.service.fetchAll()
        .then(data => {
            this.setState({dogs: data})
            this.dispatchEvent(new EventDataLoaded({test:'coucou'}));
        });
    }

    render(): void {
        if(!this.state.dogs) {
            this.shadow.textContent = 'Loading...';
            return;
        }

        const ul = document.createElement('ul');

        for (const dog of this.state.dogs) {
            const li = document.createElement('li');
            li.textContent = String(dog.name);
            ul.append(li)
        }

        const dogForm = document.createElement('simplon-dog-form');
        dogForm.addEventListener('dog-submit', (event:any) => {
            this.setState({
                dogs: [
                    ...this.state.dogs,
                    event.data
                ]
            });
        });

        this.shadow.append(ul);
        this.shadow.append(dogForm);
    }

}


customElements.define('simplon-dog', DogComponent);