
/**
 * Cette classe abstraite est là pour permettre de créer des component reactif
 * La réactivité est un principe très présent dans les frameworks frontend qui consiste
 * a faire que le component actualise son affichage tout seul à chaque fois qu'une
 * propriété "reactive" est modifiée.
 * Ici l'approche est similaire à celle de React (version classe) avec une seule
 * propriété reactive "state" qui déclenchera une actualisation de l'affichage 
 * dès qu'elle sera modifiée. Pour ce faire on crée une méthode setState par
 * laquelle il faudra obligatoirement passer pour bénéficier de la reactivité qui
 * permet de changer la valeur de state et de relancer une fonction render() chargée
 * de mettre à jour l'affichage
 */
export abstract class AbstractComponent<T> extends HTMLElement {
    shadow:ShadowRoot;
    state:T = {} as T;

    constructor() {
        super();
        /**
         * On attribue un shadow DOM à notre élément custom. Le shadow dom permet d'isoler
         * complétement le DOM de l'élément qui ne sera pas affecté par le css ou les
         * event JS de l'extérieur et dont le style et les event JS n'auront pas d'effet
         * sur le reste du DOM non plus.
         */
        this.shadow = this.attachShadow({mode:'open'});
    
    }
    /**
     * Méthode de Lifecycle qui se déclenchera automatiquement au moment où le component
     * sera append dans le HTML, on peut y mettre le premier render pour s'assurer que
     * celui ci se fasse à un moment où les constructeurs ont été lancés et où le state
     * sera complet
     */
    connectedCallback() {
        this.shadow.innerHTML = '';
        this.render();
    }
    /**
     * Méthode à appeler pour modifier l'état du component et profiter de la réactivité
     * @param value la nouvelle valeur du state
     */
    setState(value:T){
        
        this.state = value;
        
        this.shadow.innerHTML = '';
        this.render();
    }
    /**
     * Méthode d'actualisation de l'affichage du component
     */
    abstract render():void;
}