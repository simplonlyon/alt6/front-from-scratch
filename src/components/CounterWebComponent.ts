import { AbstractComponent } from "./AbstractComponent";

/**
 * Version du Counter qui utilise le AbstractComponent et donc le setState
 * pour bénéficier de la réactivité
 */
export class CounterWebComponent extends AbstractComponent<{count:number}> {

    constructor() {
        super();
        //On assigne une valeur initiale à notre state
        this.setState({count: 0});
    }

    increment() {
        //Ici on passe par le setState pour modifier la valeur de notre counter et donc déclencher un render
        this.setState({count: this.state.count+1});
    }
    decrement() {
        this.setState({count: this.state.count-1});
    }
    
    render() {
        const simplonFirst = document.createElement('simplon-first');
        this.shadow.append(simplonFirst);

        const buttonMinus = document.createElement('button');
        buttonMinus.textContent = '-';
        buttonMinus.addEventListener('click', () => {
            this.decrement();
        });
        this.shadow.append(buttonMinus);

        const span = document.createElement('span');
        span.textContent = this.state.count+'';
        this.shadow.append(span);

        const buttonPlus = document.createElement('button');
        buttonPlus.textContent = '+';
        buttonPlus.addEventListener('click', () => {
            this.increment()
        });
        this.shadow.append(buttonPlus);

        return this.shadow;

    }
}