


export class FirstWebComponent extends HTMLElement {

    constructor() {
        super();
        
        const shadow = this.attachShadow({mode: 'closed'});
        const style = document.createElement('style');

        style.innerHTML = `
            p {
                color:red;
            }
        `

        const p = document.createElement('p');
        p.textContent = 'je suis un élément custom !!';
        shadow.append(style);
        shadow.append(p);

    }
}