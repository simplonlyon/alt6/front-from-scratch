import { Person } from "../Person";
import { AbstractComponent } from "./AbstractComponent";

interface State {
    persons: Person[]
}

export class PersonComponent extends AbstractComponent<State>{

    constructor(){
        super();
        this.setState({
            persons: [
                {name:'Name 1', firstname: 'First name 1'},
                {name:'Name 2', firstname: 'First name 2'},
                {name:'Name 3', firstname: 'First name 3'},
                {name:'Name 4', firstname: 'First name 4'},
            ]
        });
    }

    render(): void {
        
        const ul = document.createElement('ul');
        for (const item of this.state.persons) {
            const li = document.createElement('li');
            li.textContent = item.name + ' '+ item.firstname;
            ul.append(li);
        }

        const button = document.createElement('button');
        button.textContent ='Add person';
        button.addEventListener('click', () =>{
            let person = {name: 'Added', firstname:'Person'};
            //Rajouter un nouvel élément dans un tableau en créant un nouveau tableau
            //qui contiendra toutes les valeurs du précédent tableau (avec le ... de destructuration qui prend toutes les valeurs d'un objet ou tableau)
            //auquel on rajoute la nouvelle valeur à la fin
            this.setState({
                persons: [
                    ...this.state.persons, 
                    person
                ]
            });
        });
        
        this.shadow.append(button);
        this.shadow.append(ul);
    }

}