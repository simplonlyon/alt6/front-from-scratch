

export class Counter {
    count = 0;
    div:HTMLDivElement|null = null;

    increment() {
        this.count++;
    }
    decrement() {
        this.count--;
    }
    
    render() {
        if(this.div == null) {
            this.div = document.createElement('div')
        }
        this.div.innerHTML = '';

        const buttonMinus = document.createElement('button');
        buttonMinus.textContent = '-';
        buttonMinus.addEventListener('click', () => {
            this.decrement();
        });
        this.div.append(buttonMinus);

        const span = document.createElement('span');
        span.dataset.cy = 'counter';
        span.textContent = this.count+'';
        this.div.append(span);

        const buttonPlus = document.createElement('button');
        buttonPlus.textContent = '+';
        buttonPlus.addEventListener('click', () => {
            this.increment()
        });
        this.div.append(buttonPlus);

        return this.div;

    }
}