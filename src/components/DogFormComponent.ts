import { Dog } from "../Dog";
import { DogService } from "../services/DogService";
import { AbstractComponent } from "./AbstractComponent";


export class EventDogSubmit extends Event {
    constructor(public data:Dog) {
        super('dog-submit');
    }
}

export class DogFormComponent extends AbstractComponent<any> {


    render(): void {
        const form = document.createElement('form');
        form.innerHTML = `
            <label>Name
                <input type="text" name="name" >
            </label>
            <label>Breed
                <input type="text" name="breed">
            </label>
            <label>birthDate
                <input type="date" name="birthDate">
            </label>
            <button>Submit</button>
        `;

        form.addEventListener('submit', async event => {
            event.preventDefault();
            const formData = new FormData(form);

            const date = formData.get('birthDate');
            const dog:Dog = {
                name: String(formData.get('name')),
                breed: String(formData.get('breed')),
                birthDate:  date ? new Date(date as string):undefined   
            }
            const service = new DogService();
            const dogResponse = await service.post(dog);
            
            this.dispatchEvent(new EventDogSubmit(dogResponse));
        })


        this.shadow.append(form);
    }

}


customElements.define('simplon-dog-form', DogFormComponent);