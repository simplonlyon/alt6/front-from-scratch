import { Student } from "./Student";


export class Promo {
    private students:Student[] = [];

    addStudent(student:Student) {
        for (const item of this.students) {
            if(item.name === student.name && item.firstName === student.firstName) {
                return;
            }
        }
        this.students.push(student);
    }
}