

function fonctionAvecPromesse(test: string) {
    return new Promise((resolve, reject) => {
        if (test.length) {
            resolve('ok');
        } else {
            reject('ko');
        }

    });
}

fonctionAvecPromesse('test')
    .then((_result) => {
        return 10;
    })
    .then(result => {
        console.log(result)
    })
    .catch((error) => console.log(error));


async function fonctionAsynchrone() {
    try {
        await fonctionAvecPromesse('test');

        return 10;
    } catch (error) {
        console.log(error);
    }
}