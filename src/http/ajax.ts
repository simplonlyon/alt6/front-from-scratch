interface XhrResponse {
    status: number;
    body: string;
    headers: string;
}
interface XhrParameter {
    url: string;
    body?: string;
    method?: string;
    callbackSuccess?: (response: XhrResponse) => void;
    callbackError?: (response: XhrResponse) => void;
}

/**
 * Fonction wrapper pour utiliser l'objet XmlHttpRequest avec des callback.
 * XmlHttpRequest est l'ancienne API pour faire des requêtes HTTP depuis le javascript,
 * il est maintenant préférable d'utiliser l'API fetch, plus simple à utiliser (ou alors
 * une library http, comme Axios)
 * Un callback est une fonction donnée en argument d'une autre fonction qui sera exécutée
 * à un moment dans la fonction en question, généralement lorsqu'une opération asynchrone
 * est terminée et que le résultat est disponible (techniquement les addEventListener utilise un callback en deuxième argument)
 * Cette manière de faire se trouve encore dans un certain nombre de library et en particulier
 * côté node.js mais elle coexiste souvent avec une version Promise
 * @param config Les options de configurations nécessaires pour faire la requête (l'url, la méthode, etc.) 
 */
export function doXhr({
    url,
    body = '',
    method = 'GET',
    callbackSuccess = () => { },
    callbackError = () => { }
}: XhrParameter) {
    const httpRequest = new XMLHttpRequest();

    httpRequest.onreadystatechange = function () {
        console.log('readyState', httpRequest.readyState);
        if (httpRequest.readyState == httpRequest.DONE) {
            if (httpRequest.status >= 200 && httpRequest.status < 400) {
                console.log(httpRequest.getAllResponseHeaders());
                callbackSuccess({
                    status: httpRequest.status,
                    headers: httpRequest.getAllResponseHeaders(),
                    body: httpRequest.response
                });

            } else {
                callbackError({
                    status: httpRequest.status,
                    headers: httpRequest.getAllResponseHeaders(),
                    body: httpRequest.response
                });
            }

        }
    }

    httpRequest.open(method, url);

    httpRequest.send(body);

}

/**
 * Même fonction qu'au dessus, mais en remplaçant les callback par une Promise.
 * L'API Promise permet de gérer l'asynchrone de manière plus fine et modulable,
 * le principe est que la fonction renvoie un objet promise sur lequel on pourra
 * lancer un ou des .then() qui s'exécuteront lorsque le résultat d'une opération
 * asynchrone sera disponible. Pour faire un wrapper Promise, on fait un return d'une
 * instance de promise qui déclenchera une fonction resolve ou reject selon si l'opération
 * réussie ou non.
 * Un des avantage des Promise est la gestion généralisée des erreurs à l'intérieur des .catch(),
 * le fait de pouvoir chaîner des .then pour changer la valeur de celui ci au fur et à mesure, et le
 * fait que maintenant le langage JS inclus le sucre syntaxique async/await qui permet d'écrire
 * du code asynchrone presque comme du code synchrone
 * @returns La promesse qui contiendra la réponse à notre requête
 */
export function doXhrPromise({
    url,
    body = '',
    method = 'GET'
}: XhrParameter) {
    return new Promise<XhrResponse>((resolve, reject) => {

        const httpRequest = new XMLHttpRequest();

        httpRequest.onreadystatechange = function () {
            console.log('readyState', httpRequest.readyState);
            if (httpRequest.readyState == httpRequest.DONE) {
                if (httpRequest.status >= 200 && httpRequest.status < 400) {
                    
                    resolve({
                        status: httpRequest.status,
                        headers: httpRequest.getAllResponseHeaders(),
                        body: httpRequest.response
                    });

                } else {
                    reject({
                        status: httpRequest.status,
                        headers: httpRequest.getAllResponseHeaders(),
                        body: httpRequest.response
                    });
                }

            }
        }

        httpRequest.open(method, url);

        httpRequest.send(body);

    })
}