import { doXhrPromise} from './http/ajax';


// getHtml();


fetch('http://localhost:8080/dogs')
.then(response => response.json())
.then(body => console.log(body));
 
export async function getHtml() {
    const response = await doXhrPromise({url:'http://localhost:5173'});

    document.body.innerHTML = response.body;
}